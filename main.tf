# Definiendo el provider
provider "digitalocean" {
  # token = $DIGITALOCEAN_TOKEN
}

# Usando mi clave pública que esta
# en DigitalOcean.
data "digitalocean_ssh_key" "notebook" {
  name = "gregorip02@notebook"
}

data "digitalocean_ssh_key" "gitlab" {
  name = "gregorip02@gitlab.com/about.git"
}

# Para agregar una nueva clave publica
# resource "digitalocean_ssh_key" "ssh" {
#  name = "gregorip02@notebook"
#  # Recuerda copiar el archivo id_rsa.pub al workdirectory
#  public_key= file("id_rsa.pub")
#}

# Creando el droplet
resource "digitalocean_droplet" "droplet" {
  name       = "gregori.com.ve"
  region     = "nyc1"
  monitoring = true
  size       = "s-1vcpu-1gb"
  image      = "docker-18-04"
  # user_data = file("userdata.yaml")
  ssh_keys   = [
    data.digitalocean_ssh_key.gitlab.fingerprint,
    data.digitalocean_ssh_key.notebook.fingerprint
  ]
}

# Creando el dominio para el droplet
resource "digitalocean_domain" "domain" {
  name = "gregori.com.ve"
}

# Creando los registros dns
resource "digitalocean_record" "a_record" {
  domain = digitalocean_domain.domain.name
  type   = "A"
  name   = "@"
  ttl    = "3600"
  value  = digitalocean_droplet.droplet.ipv4_address
}

resource "digitalocean_record" "cname_record" {
  domain = digitalocean_domain.domain.name
  type   = "CNAME"
  name   = "www"
  ttl    = "43200"
  value  = "${digitalocean_domain.domain.name}."
}

# Creando los certificados mediante letsencrypt
/*resource "digitalocean_certificate" "cert" {
  name    = digitalocean_domain.domain.name
  type    = "lets_encrypt"
  domains = [
    digitalocean_domain.domain.name,
    "www.${digitalocean_domain.domain.name}"
  ]
}


# Crear un balanceador de carga
resource "digitalocean_loadbalancer" "loadbalancer" {
  redirect_http_to_https = true
  name = digitalocean_domain.domain.name
  region = "nyc1"

  forwarding_rule {
    entry_port = 443
    entry_protocol = "https"

    target_port = 80
    target_protocol = "http"

    certificate_id = digitalocean_certificate.cert.id
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_ids = [digitalocean_droplet.droplet.id]
}*/
